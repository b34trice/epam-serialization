﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task.DB;
using Task.TestHelpers;
using System.Runtime.Serialization;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using Task.Services;
using Task.Data;

namespace Task
{
	[TestClass]
	public class SerializationSolutions
	{
		Northwind dbContext;

		[TestInitialize]
		public void Initialize()
		{
			dbContext = new Northwind();
		}

		[TestMethod]
		public void SerializationCallbacks()
		{
			dbContext.Configuration.ProxyCreationEnabled = false;

			var serializationContext = new Context
			{
				ObjectContext = (dbContext as IObjectContextAdapter).ObjectContext,
				TypeToSerialize = typeof(Category)
			};
			var xmlSerializer = new NetDataContractSerializer(new StreamingContext(StreamingContextStates.All, serializationContext));
			var serializer = new XmlSerializer<IEnumerable<Category>>(xmlSerializer);


			var tester = new Serializer<IEnumerable<Category>>(serializer);
			var categories = dbContext.Categories.ToList();

			var c = categories.First();

			tester.SerializeAndDeserialize(categories);
		}

		[TestMethod]
		public void ISerializable()
		{
			dbContext.Configuration.ProxyCreationEnabled = false;

			var serializationContext = new Context
			{
				ObjectContext = (dbContext as IObjectContextAdapter).ObjectContext,
				TypeToSerialize = typeof(Product)
			};

			var xmlSerializer = new NetDataContractSerializer(new StreamingContext(StreamingContextStates.All, serializationContext));
			var serializer = new XmlSerializer<IEnumerable<Product>>(xmlSerializer);
			var tester = new Serializer<IEnumerable<Product>>(serializer);

			tester.SerializeAndDeserialize(dbContext.Products);
		}


		[TestMethod]
		public void ISerializationSurrogate()
		{
			dbContext.Configuration.ProxyCreationEnabled = false;

			var serializationContext = new Context
			{
				ObjectContext = (dbContext as IObjectContextAdapter).ObjectContext,
				TypeToSerialize = typeof(Order_Detail)
			};

			var xmlSerializer = new NetDataContractSerializer()
			{
				SurrogateSelector = new Selector(),
				Context = new StreamingContext(StreamingContextStates.All, serializationContext)
			};
			var serializer = new XmlSerializer<IEnumerable<Order_Detail>>(xmlSerializer);
			var tester = new Serializer<IEnumerable<Order_Detail>>(serializer);
			var orderDetails = dbContext.Order_Details.ToList();

			tester.SerializeAndDeserialize(orderDetails);
		}

		[TestMethod]
		public void IDataContractSurrogate()
		{
			dbContext.Configuration.ProxyCreationEnabled = true;
			dbContext.Configuration.LazyLoadingEnabled = true;

			var xmlSerializer = new DataContractSerializer(typeof(IEnumerable<Order>),
				new DataContractSerializerSettings
				{
					DataContractSurrogate = new ContractSurrogate()
				});

			var serializer = new XmlSerializer<IEnumerable<Order>>(xmlSerializer);
			var tester = new Serializer<IEnumerable<Order>>(serializer);

			tester.SerializeAndDeserialize(dbContext.Orders);
		}
	}
}
