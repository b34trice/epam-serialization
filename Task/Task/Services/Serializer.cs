﻿using System.IO;

namespace Task.Services
{
    public class Serializer<T>
    {
        private readonly XmlSerializer<T> serializer;

        public Serializer(XmlSerializer<T> serializer)
        {
            this.serializer = serializer;
        }

        public T SerializeAndDeserialize(T data)
        {
            using (var stream = new MemoryStream())
            {
                serializer.Serialize(data, stream);
                stream.Seek(0, SeekOrigin.Begin);
                var result = serializer.Deserialize(stream);
                return result;
            }
        }
    }
}