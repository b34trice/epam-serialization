﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Task1
{
    [XmlRoot(ElementName = "catalog", Namespace = "http://library.by/catalog")]
    public class Catalog
    {
        [XmlAttribute(AttributeName = "date", DataType = "date")]
        public DateTime Date { get; set; }

        /*
         * If use IEnumerable type - throw XmlReflextion exception
         * There was an error reflecting type
         */
        [XmlElement("book")]
        public List<Book> Books { get; set; }
    }
}