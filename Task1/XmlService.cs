﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Task1
{
    public class XmlService
    {
        private readonly XmlSerializer _serializer;

        public XmlService()
        {
            _serializer = new XmlSerializer(typeof(Catalog));
        }

        public void Serialize(Catalog model)
        {
            using (var xmlWriter = XmlWriter.Create("BooksResult.xml", new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                Indent = true
            }))
            {
                _serializer.Serialize(xmlWriter, model);
            }
        }

        public Catalog Deserialize(string path)
        {
            using (var fileStream = File.OpenRead(path))
            {
                return (Catalog)_serializer.Deserialize(fileStream);
            }
        }
    }
}