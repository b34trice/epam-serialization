﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task1;

namespace Task1Test
{
	[TestClass]
	public class XmlServiceUnitTests
	{
		private readonly XmlService _service;

		public XmlServiceUnitTests()
		{
			_service = new XmlService();
		}

		[TestMethod]
		public void DeserializeUnitTest_GetValidModel()
		{
			var result = _service.Deserialize("books.xml");
			Assert.AreEqual(12, result.Books.Count);
			Assert.AreEqual(DateTime.Parse("2/5/2016 12:00:00 AM"), result.Date);
		}
	}
}
